import React, { Component } from "react";
import "./styles.css";
import Board from "./components/Board";
import Score from "./components/Score";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import ApolloClient from "apollo-boost";
import { ApolloProvider } from "react-apollo";
import Login from "./components/Login";

const client = new ApolloClient({
  uri: "https://server-zytgwyhhls.now.sh/"
});

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    return (
      <ApolloProvider client={client}>
        <Router>
          <div>
            <nav className="nav-container">
              <ul className="links-container">
                <li className="links-container_items">
                  <Link to="/board">Board</Link>
                </li>
                <li className="links-container_items">
                  <Link to="/score">Score</Link>
                </li>
              </ul>
            </nav>

            <Route
              exact={true}
              path="/"
              render={props => <Login userCreated={this.userCreated} />}
            />
            <Route path="/score" component={Score} />
            <Route path="/board" render={props => <Board {...props} />} />
          </div>
        </Router>
      </ApolloProvider>
    );
  }
}

export default App;
