import { gql } from "apollo-boost";
const UPDATE_USER = gql`
  mutation($userId: ID!, $score: Int, $word: String!) {
    updateUser(
      where: { id: $userId }
      data: { score: $score, words: { create: { word: $word } } }
    ) {
      id
      score
      username
      words {
        word
      }
    }
  }
`;

const SIGNUP = gql`
  mutation($username: String!) {
    createUser(username: $username) {
      token
      user {
        id
        username
      }
    }
  }
`;

const LOGIN = gql`
  mutation($username: String!, $password: String!) {
    login(username: $username, password: $password) {
      token
      user {
        id
        username
      }
    }
  }
`;
export { UPDATE_USER, SIGNUP, LOGIN };
