import { gql } from "apollo-boost";

const QUERY_USERS = gql`
  query {
    users {
      id
      username
      score
    }
  }
`;

export { QUERY_USERS };
