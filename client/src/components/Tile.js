import React, { Component } from "react";

class Tile extends Component {
  render() {
    const { letter, index, addToTextField, selectedTile } = this.props;
    return (
      <li
        id={`item${index}`}
        className={`board__items`}
        onClick={e => {
          addToTextField(letter);
          selectedTile(e);
        }}
        ref={liElem => (this.liElem = liElem)}
      >
        {letter}
      </li>
    );
  }
}

export default Tile;
