import React, { Component } from "react";
import { Query } from "react-apollo";
import { QUERY_USERS } from "../graphql/queries";

class Score extends Component {
  render() {
    return (
      <Query query={QUERY_USERS}>
        {({ loading, error, data }) => {
          if (loading) {
            return <div>Loading...</div>;
          }
          if (error) {
            return <div>{error}</div>;
          }

          return (
            <section className="score-section">
              <h1 className="title">Score</h1>
              <ul className="list-container">
                {data.users.map(i => (
                  <li key={`user-item${i.id}`} className="list-item">
                    {i.username}{" "}
                    <span style={{ fontSize: "36px" }}>
                      {i.score}
                      pts
                    </span>
                  </li>
                ))}
              </ul>
            </section>
          );
        }}
      </Query>
    );
  }
}

export default Score;
