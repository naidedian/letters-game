import React, { Component } from "react";

class TextField extends Component {
  render() {
    const { isValid, letters } = this.props;
    return (
      <div className="textfield-container">
        <div className={`isvalid-text ${isValid ? "valid" : ""}`}>
          {isValid ? "valid" : "invalid"}
        </div>
        <input
          type="text"
          value={letters.map(i => i.letter).join("")}
          readOnly
          className={`word-textfield ${isValid ? "valid" : ""}`}
        />
        {isValid}
      </div>
    );
  }
}

export default TextField;
