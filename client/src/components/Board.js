import React, { Component } from "react";
import { compose, graphql } from "react-apollo";
import { QUERY_USERS } from "../graphql/queries";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimesCircle } from "@fortawesome/free-solid-svg-icons";
import { UPDATE_USER } from "../graphql/mutation";

import Tile from "./Tile";
import TextField from "./TextField";

class Board extends Component {
  constructor(props) {
    super(props);

    this.state = {
      board: [],
      word: { letters: [] },
      dictionary: [],
      showModal: true,
      isValid: false,
      isTileSelected: false,
      userInfo: {}
    };
  }

  componentWillMount() {
    this.validateCacheStorage();
  }

  componentDidMount() {
    this.fetchBoard(); // initial board fetch
    this.fetchDictionary(); // initial dictionary fetch
  }

  /** Gets all letters from server */
  fetchBoard = async () => {
    const data = await fetch(
      "https://server-zytgwyhhls.now.sh/files/test-board-2.json"
    );
    const { board } = await data.json();

    // this method will shuffle the board
    this.shuffleBoard(board);
  };

  shuffleBoard = board => {
    const indexArr = [];
    const newArr = [];

    board.map((item, index) => {
      return indexArr.push(index);
    });

    indexArr.sort(() => 0.5 - Math.random());

    this.setState(prevState => {
      board.map((item, index) => {
        return (newArr[indexArr.shift()] = item);
      });

      return {
        board: [...prevState.board, ...newArr]
      };
    });
  };

  /** Gets all words from server */
  fetchDictionary = async () => {
    const data = await fetch(
      "https://server-zytgwyhhls.now.sh/files/dictionary.json"
    );
    const { words } = await data.json();
    const dictionary = words;

    this.setState(() => {
      return {
        dictionary: [...this.state.dictionary, ...dictionary]
      };
    });
  };

  /** Adds letters to input field */
  addToTextField = (letter, e) => {
    if (!(this.state.word.length >= 6)) {
      this.setState(prevState => {
        return {
          word: {
            letters: [...prevState.word.letters, { letter }]
          }
        };
      }, this.validateWordExistence);
    }
  };

  /** Validates if word exists in dictionary */
  validateWordExistence = () => {
    const { updateUser } = this.props;
    const userId = this.state.userInfo.user.id;
    const username = this.state.userInfo.user.username;

    const word = this.state.word.letters
      .map(i => {
        return i.letter;
      })
      .join("");

    this.state.dictionary.map(dic => {
      // console.log("word", word);

      if (word.toLowerCase() === dic) {
        this.setState(
          () => {
            return {
              isValid: true
            };
          },
          async () => {
            await updateUser({
              variables: {
                userId,
                word,
                username
              }
            });
          }
        );
      }
    });
  };

  resetInput = () => {
    this.setState({ word: { letters: [] } });
    this.setState(() => ({ isValid: false }), this.removeClassName);
  };

  removeClassName = () => {
    const invalidElem = Array.from(document.querySelectorAll(".board__items"));

    invalidElem.map(i => {
      i.classList.remove("tile-invalid-selected");
      i.classList.remove("tile-valid-selected");
    });
  };

  selectedTile = e => {
    const boardItem = e.currentTarget;

    console.log(this.state.isValid);

    if (!boardItem.classList.contains("tile-invalid-selected")) {
      boardItem.classList.add("tile-invalid-selected");
    } else {
      boardItem.classList.remove("tile-invalid-selected");
      this.removeLetter(boardItem.innerHTML);
    }

    if (this.state.isValid) {
      const elemArray = Array.from(
        document.querySelectorAll(".tile-invalid-selected")
      );

      elemArray.map(i => {
        i.classList.remove("tile-invalid-selected");
        i.classList.add("tile-valid-selected");
      });
    }

    // if (!boardItem.classList.contains("tile-invalid-selected")) {
    //   if (this.state.isValid) {
    //     // boardItem.classList.remove("tile-invalid-selected");
    //     const elemArray = Array.from(
    //       document.querySelectorAll(".tile-invalid-selected")
    //     );
    //     console.log(elemArray);

    //     elemArray.map(i => {
    //       i.classList.remove("tile-invalid-selected");
    //       i.classList.add("tile-valid-selected");
    //     });
    //   } else {
    //     boardItem.classList.remove("tile-valid-selected");
    //     boardItem.classList.add("tile-invalid-selected");
    //   }
    // } else {
    //   boardItem.classList.remove("tile-invalid-selected");

    //   this.removeLetter(boardItem.innerHTML);
    // }
  };

  removeLetter = letter => {
    const letters = this.state.word.letters.map(item => item.letter);
    // console.log(letter, letters.indexOf(letter));

    if (letters.indexOf(letter) >= 0) {
      this.setState(prevState => {
        return {
          word: {
            letters: [
              ...this.state.word.letters.filter(
                (_, i) => i !== letters.indexOf(letter) /*_.letter !== letter*/
              )
            ]
          }
        };
      });
    }
  };

  validateCacheStorage = async () => {
    const cacheStored = localStorage.getItem("userInfo");

    if (cacheStored) {
      this.setState({
        userInfo: JSON.parse(cacheStored)
      });
      return;
    }

    const userInfo = this.props.location.state.userInfo;
    localStorage.setItem("userInfo", JSON.stringify(userInfo));

    this.setState(() => ({
      userInfo: userInfo
    }));
  };

  render() {
    if (!Object.keys(this.state.userInfo).length) {
      // alert("You need to create a user before playing.");

      return (window.location.href = "/");
    }

    const { username } = this.state.userInfo.user;
    return (
      <div className="flex-container">
        <div className="main">
          <div style={{ textAlign: "right" }}>
            Welcome{" "}
            <span style={{ fontSize: "22px", color: "#888" }}>{username}</span>
          </div>
          <a
            className="reset-board"
            href="/"
            onClick={e => {
              e.preventDefault();
              this.resetInput();
            }}
            disabled={this.state.word.letters.length ? false : true}
          >
            <span className="icon-text">Clear word</span>
            <FontAwesomeIcon icon={faTimesCircle} />
          </a>
          <div className="board-container">
            <ul className="board">
              {this.state.board.map((letter, index) => (
                <Tile
                  tileValid={this.state.isValid}
                  key={`item${index}`}
                  letter={letter}
                  index={index}
                  addToTextField={this.addToTextField}
                  selectedTile={this.selectedTile}
                />
              ))}
            </ul>
            <TextField
              isValid={this.state.isValid}
              letters={this.state.word.letters}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default compose(
  graphql(QUERY_USERS, { name: "users" }),
  graphql(UPDATE_USER, { name: "updateUser" })
)(Board);
