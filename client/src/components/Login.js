import React, { Component } from "react";
import { compose, graphql } from "react-apollo";
import { SIGNUP } from "../graphql/mutation";
import SweetAlert from "react-bootstrap-sweetalert";

import { Redirect } from "react-router-dom";
class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      redirectToBoard: false,
      userInfo: {}
    };
  }

  componentDidMount() {
    localStorage.clear();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.userInfo !== this.state.userInfo) {
      this.setState(() => {
        return {
          redirectToBoard: true
        };
      });
    }
  }

  userCreated = data => {
    const userData = data.createUser;

    this.setState(() => {
      return {
        userInfo: userData
      };
    });
  };

  onConfirm = async username => {
    const { data } = await this.props.signup({
      variables: { username }
    });

    this.userCreated(data);
  };

  render() {
    const { userInfo } = this.state;

    if (this.state.redirectToBoard) {
      return <Redirect to={{ pathname: "/board", state: { userInfo } }} />;
    }

    return (
      <SweetAlert
        input
        title="Enter your username:"
        confirmBtnStyle={{
          backgroundColor: "orange",
          color: "#fff",
          border: "none",
          padding: "10px",
          borderRadius: "3px",
          width: "60px",
          tapHighlightColor: "transparent"
        }}
        confirmBtnText="Log in"
        onConfirm={this.onConfirm}
      />
    );
  }
}

export default compose(graphql(SIGNUP, { name: "signup" }))(Login);
