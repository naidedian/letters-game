_We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket._

---

## Initial Setup

Follow this instructions to get the game up and running

1. Open your console or terminal and run
   `git clone https://naidedian@bitbucket.org/naidedian/letters-game.git`
2. Type `cd letters-game/client`
3. Type `yarn or npm install`
4. After that run dev server by typing `yarn dev or npm run dev`
5. Start playing the game.

---
