require("dotenv").config();
const express = require("express");
const { Prisma } = require("prisma-binding");
const cors = require("cors");
const { GraphQLServer } = require("graphql-yoga");
const bodyParser = require("body-parser");
const path = require("path");
const queries = require("./src/resolvers/queries");
const mutations = require("./src/resolvers/mutations");
const PORT = process.env.PORT || 3001;

const resolvers = {
  ...queries,
  ...mutations
};

const server = new GraphQLServer({
  typeDefs: "./src/schema.graphql",
  resolvers,
  context: req => ({
    ...req,
    db: new Prisma({
      typeDefs: "./src/generated/prisma.graphql",
      endpoint: process.env.PRISMA_DEV_ENDPOINT,
      secret: process.env.PRISMA_SECRET,
      debug: false
    })
  })
});

server.express.use(bodyParser.json());
server.express.use(cors("*", "http://localhost:3000"));
server.express.use(express.static(path.join(__dirname, "public")));

server.start(({ port }) =>
  console.log(`Listening to http://localhost:${port}`)
);
