const jwt = require("jsonwebtoken");
const jwtSecret = process.env.JWT_SECRET;
getUserId = ctx => {
  const Authorization = ctx.request.get("Authorization");
  if (Authorization) {
    const token = Authorization.replace("Bearer ", "");
    const { userId } = jwt.verify(token, jwtSecret);
    return userId;
  }

  throw new Error("Not authenticated.");
};

module.exports = {
  getUserId,
  jwtSecret
};
