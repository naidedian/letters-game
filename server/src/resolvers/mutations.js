const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const mutations = {
  Mutation: {
    createUser: async (parent, args, ctx, info) => {
      // const password = await bcrypt.hash(args.password, 10);

      console.log("heyyy");

      const user = await ctx.db.mutation.createUser({
        data: { ...args }
      });

      const token = jwt.sign({ userId: user.id }, process.env.JWT_SECRET);

      return {
        token,
        user
      };
    },
    updateUser: async (parent, args, ctx, info) => {
      const user = await ctx.db.query.user(
        { where: { ...args.where } },
        `{username score id words {word}}`
      );

      const { aggregate } = await ctx.db.query.wordsConnection(
        {},
        `{aggregate{count}}`
      );

      const data = await ctx.db.mutation.updateUser(
        {
          where: { id: user.id },
          data: {
            words: { create: args.data.words.create.map(i => i) },
            username: user.username,
            score: user.words.length + 1
          }
        },
        info
      );

      return data;
    }
  }
};

module.exports = mutations;
