const queries = {
  Query: {
    words: async (parent, args, ctx, info) => {
      return await ctx.db.query.words({}, info);
    },
    users: async (parent, args, ctx, info) => {
      return await ctx.db.query.users({}, info);
    },
    wordsConnection: async (parent, args, ctx, info) => {
      const data = await ctx.db.query.wordsConnection({}, info);

      return data;
    }
  }
};

module.exports = queries;
